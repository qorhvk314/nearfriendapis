package co.kr.psqcloud.api.member.repository;

import co.kr.psqcloud.api.member.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Long> {

    @Query(
            value = "select * from member where earth_distance(ll_to_earth(posy, posx), ll_to_earth(:pointY, :pointX)) <= :searchDistance",
            nativeQuery = true
    )
    List<Member> findAllByNearFriends(@Param("pointY") double pointY, @Param("pointX") double pointX, @Param("searchDistance") double searchDistance);

    @Query(
            value = "select * from member where earth_distance(ll_to_earth(posy, posx), ll_to_earth(:pointY, :pointX)) <= :searchDistance and member.gender = :gender",
            nativeQuery = true
    )
    List<Member> findAllByNearFriendsByGender(@Param("pointY") double pointY, @Param("pointX") double pointX, @Param("searchDistance") double searchDistance, @Param("gender") String gender);
}
