package co.kr.psqcloud.api.member.model;

import co.kr.psqcloud.api.member.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberCreateRequest {
    @ApiModelProperty(notes = "닉네임")
    @NotNull
    @Length(min = 2, max = 20)
    private String nickname;

    @ApiModelProperty(notes = "취미")
    @NotNull
    @Length(min = 2, max = 50)
    private String hobby;

    @ApiModelProperty(notes = "성별")
    @NotNull
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ApiModelProperty(notes = "위치 x좌표")
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "위치 y좌표")
    @NotNull
    private Double posY;
}
