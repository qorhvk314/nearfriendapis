package co.kr.psqcloud.api.member.service;

import co.kr.psqcloud.api.member.entity.Member;
import co.kr.psqcloud.api.member.model.MemberCreateRequest;
import co.kr.psqcloud.api.member.model.NearFriendItem;
import co.kr.psqcloud.api.member.repository.MemberRepository;
import co.kr.psqcloud.common.response.model.ListResult;
import co.kr.psqcloud.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    @PersistenceContext
    EntityManager entityManager;


    public void setMember(MemberCreateRequest createRequest) {
        Member member = new Member.Builder(createRequest).build();
        memberRepository.save(member);
    }


    /**
     * 근처 친구 리스트를 가져온다.
     *
     * @param posX ex 126.머시기....
     * @param posY ex 37.머시기....
     * @param distance 몇km의 친구 리스트를 가져올지.. ex 1, 2, 3
     * @return
     */
    public ListResult<NearFriendItem> getNearFriends(double posX, double posY, int distance) {
        double distanceResult = distance * 1000; // 미터 기준으로 변환.. x 1000

        String queryString = "select * from public.get_near_friends(" + posX + ", " + posY + ", " + distanceResult + ")"; // 쿼리 문자열(String)로 준비하기
        Query nativeQuery = entityManager.createNativeQuery(queryString); // entityManager(DB 담당하는애)한테 위에서 작성한 쿼리모양 문자열을 쿼리 객체로 바꿔주기
        List<Object[]> resultList = nativeQuery.getResultList(); // 쿼리 실행해서 리스트 가져오기.. 근데 얘는 모양이 확정된애가 아니라서 object(정해지지않은 객체)로 가져오기

        List<NearFriendItem> result = new LinkedList<>(); // 결과값 담을 빈 리스트 준비
        for (Object[] resultItem : resultList) { // 쿼리 실행해서 가져온 리스트에서 한줄씩 던져주면서
            result.add( // 결과값 담을 리스트에 추가해준다.
                    new NearFriendItem.Builder( // NearFriendItem 모양으로 담아야 하니까 NearFriendItem에 빌더 호출
                            resultItem[0].toString(), // 모양이 정해지지 않은 객체이므로 이건 String이다 라고 확정시켜주기
                            resultItem[1].toString(), // 모양이 정해지지 않은 객체이므로 이건 String이다 라고 확정시켜주기
                            resultItem[2].toString(), // 모양이 정해지지 않은 객체이므로 이건 String이다 라고 확정시켜주기
                            Double.parseDouble(resultItem[3].toString())).build() // 모양이 정해지지 않은 객체이므로 이건 Double 이다 라고 확정시켜주기
            );
        }

        return ListConvertService.settingResult(result); // 결과값이 다 완성되었으므로 돌려주기
    }
}
