package co.kr.psqcloud.api.member;

import co.kr.psqcloud.api.member.entity.Member;
import co.kr.psqcloud.api.member.enums.Gender;
import co.kr.psqcloud.api.member.repository.MemberRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class ApiMemberApplicationTests {
    @Autowired
    MemberRepository memberRepository;

    @Test
    void contextLoads() {

    }

    @Test
    void dataTest1() {
        double searchX = 126.835639929462;
        double searchY = 37.3179417691029;
        double searchDistance = 200;

        List<Member> result = memberRepository.findAllByNearFriends(searchY, searchX, searchDistance);

        int test = 1;
    }

    @Test
    void dataTest2() {
        double searchX = 126.835639929462;
        double searchY = 37.3179417691029;
        double searchDistance = 200;
        String searchGender = Gender.WOMAN.toString();

        List<Member> result = memberRepository.findAllByNearFriendsByGender(searchY, searchX, searchDistance, searchGender);

        int test = 1;
    }
}
