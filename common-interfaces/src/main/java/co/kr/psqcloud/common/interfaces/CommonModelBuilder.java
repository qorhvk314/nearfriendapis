package co.kr.psqcloud.common.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
