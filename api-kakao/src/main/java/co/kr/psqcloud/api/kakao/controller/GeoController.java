package co.kr.psqcloud.api.kakao.controller;

import co.kr.psqcloud.api.kakao.model.ConvertAddressResponse;
import co.kr.psqcloud.api.kakao.model.SearchAddressResponse;
import co.kr.psqcloud.api.kakao.model.SearchRegionResponse;
import co.kr.psqcloud.api.kakao.service.GeoService;
import co.kr.psqcloud.common.response.model.SingleResult;
import co.kr.psqcloud.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "좌표 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/geo")
public class GeoController {
    private final GeoService geoService;

    @ApiOperation(value = "주소를 좌표로 변환하기")
    @GetMapping("/search/address")
    public SingleResult<SearchAddressResponse> getSearchAddress(@RequestParam("searchAddress") String searchAddress) {
        return ResponseService.getSingleResult(geoService.getSearchAddress(searchAddress));
    }

    @ApiOperation(value = "행정구역정보 검색하기")
    @GetMapping("/search/region")
    public SingleResult<SearchRegionResponse> getSearchRegion(@RequestParam("x") String x, @RequestParam("y") String y) {
        return ResponseService.getSingleResult(geoService.getSearchRegion(x, y));
    }

    @ApiOperation(value = "주소 변환하기")
    @GetMapping("/convert/address")
    public SingleResult<ConvertAddressResponse> getConvertAddress(@RequestParam("x") String x, @RequestParam("y") String y) {
        return ResponseService.getSingleResult(geoService.getConvertAddress(x, y));
    }
}
